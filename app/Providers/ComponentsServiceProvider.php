<?php

namespace App\Providers;

use Collective\Html\FormFacade as Form;
use Illuminate\Support\ServiceProvider;

class ComponentsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //////////////
        // Checkbox //
        //////////////
        Form::component(
            'bsCheckbox',
            'components.form.checkbox',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        //////////
        // CNPJ //
        //////////
        Form::component(
            'bsCnpj',
            'components.form.cnpj',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        /////////
        // CPF //
        /////////
        Form::component(
            'bsCpf',
            'components.form.cpf',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        //////////
        // Date //
        //////////
        Form::component(
            'bsDate',
            'components.form.date',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => [],
            ]
        );

        //////////////
        // Datetime //
        //////////////
        Form::component(
            'bsDatetime',
            'components.form.datetime',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => [],
            ]
        );

        //////////////////
        // Dynamic Text //
        //////////////////
        Form::component(
            'bsDynamicText',
            'components.form.dynamic_text',
            [
                'name',
                'label' => null,
                'values' => [],
                'attributes' => []
            ]
        );

        ////////////
        // E-mail //
        ////////////
        Form::component(
            'bsEmail',
            'components.form.email',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        ////////////
        // Number //
        ////////////
        Form::component(
            'bsNumber',
            'components.form.number',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        //////////////
        // Password //
        //////////////
        Form::component(
            'bsPassword',
            'components.form.password',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        ////////////
        // Phone //
        ////////////
        Form::component(
            'bsPhone',
            'components.form.phone',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        ////////////
        // Phones //
        ////////////
        Form::component(
            'bsPhones',
            'components.form.phones',
            [
                'name',
                'label' => null,
                'values' => [],
                'attributes' => []
            ]
        );

        ////////
        // RG //
        ////////
        Form::component(
            'bsRg',
            'components.form.rg',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        Form::component(
            'bsSelect',
            'components.form.select',
            [
                'name',
                'label' => null,
                'options' => [],
                'value' => null,
                'attributes' => []
            ]
        );

        ////////////
        // Submit //
        ////////////
        Form::component(
            'bsSubmit',
            'components.form.submit',
            [
                'value',
                'attributes' => []
            ]
        );

        //////////
        // Text //
        //////////
        Form::component(
            'bsText',
            'components.form.text',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );

        ///////////////
        // Text area //
        ///////////////
        Form::component(
            'bsTextarea',
            'components.form.textarea',
            [
                'name',
                'label' => null,
                'value' => null,
                'attributes' => []
            ]
        );
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
