<div class="form-group">
    {{ Form::label($name, $label, ['class' => 'form-control-label']) }}
    {{ Form::checkbox($name, true, $value) }}
    {!! $errors->first($name, '<p class="invalid-feedback">:message</p>')  !!}
</div>
