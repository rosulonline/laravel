<div class="form-group">
    {{ Form::label($name, $label, ['class' => 'form-control-label']) }}
    {{ Form::select($name, array_except($options, ['new']), $value, array_merge(['class' => $errors->has($name) ? 'form-control is-invalid' : 'form-control', 'style' => 'width: 100%'], $attributes)) }}
    {!! $errors->first($name, '<p class="invalid-feedback">:message</p>')  !!}
</div>

@push('js-helpers')
    <script>
        $('#{{ $name }}').select2({
            theme: "bootstrap4",
            language: "{{ app()->getLocale() }}",
            debug: true,
        });
    </script>
@endpush
