<div class="form-group">
    {{ Form::label($name, $label, ['class' => 'form-control-label']) }}
    <div class="input-group date" id="{{ $name }}-dt">
        {{ Form::text($name, $value, array_merge(['class' => $errors->has($name) ? 'form-control is-invalid' : 'form-control'], $attributes)) }}
        <div class="input-group-append">
            <span class="input-group-text">
                <i class="oi oi-calendar"></i>
            </span>
        </div>
    </div>
    {!! $errors->first($name, '<p class="invalid-feedback">:message</p>')  !!}
</div>

@push('js-helpers')
    <script>
        $(function () {
            $('#{{ $name }}-dt').datetimepicker({
                locale: '{{ app()->getLocale() }}'
            });
        });
    </script>
@endpush
